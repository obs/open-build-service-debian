#!/bin/sh

if [ -z "$OBS_FRONTEND_HOST" ] || [ -z "$OBS_BACKEND_HOST" ]
then
    echo >&2 'You need to specify OBS_FRONTEND_HOST and OBS_BACKEND_HOST'
    exit 1
fi

if [ "$(stat -c %U /srv/obs/run)" != obsrun ]
then
    echo "OBS files owned by the wrong user $(stat -c %U /srv/obs/run), re-owning..."
    chown obsrun:obsrun -R /srv/obs
fi

if [ ! -f /etc/obs/BSConfig.pm ]
then
    echo "OBS backend configuration not found, starting from scratch"
    cp /usr/lib/obs/server/BSConfig.pm.template /etc/obs/BSConfig.pm
fi

echo "Configure OBS backend host: $OBS_BACKEND_HOST"
sed -i "s/hostname = .*/hostname = '$OBS_BACKEND_HOST';/g" /etc/obs/BSConfig.pm

echo "Configure OBS frontend host: ${OBS_FRONTEND_HOST}"
sed -i "s/frontend = undef/frontend = '${OBS_FRONTEND_HOST}'/g" /etc/obs/BSConfig.pm

for arch in ${OBS_ARCHES:-x86_64 i586 armv7hl aarch64}
do
    for template in /opt/services/*@.conf.in
    do
        conf=$(echo $(basename $template) | sed -e "s|@|@$arch|" -e 's|.in$||')
        sed -e "s|@ARCH@|$arch|g" $template > /etc/supervisor/conf.d/$conf
    done
done

mkdir -p /srv/obs/log

/usr/bin/supervisord -n
