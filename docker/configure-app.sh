#!/bin/sh

if [ -z "$OBS_BACKEND_HOST" ]; then
    echo >&2 'error: server backend is unavailable and hostname option is not specified '
    echo >&2 '  You need to specify OBS_BACKEND_HOST'
    exit 1
fi

# Place api and repo url on index page
FQHOSTNAME=$(hostname)
sed -e "s,___API_URL___,https://$FQHOSTNAME,g" \
    -e "s,___REPO_URL___,http://$FQHOSTNAME:82,g" \
    /usr/share/obs/overview/overview.html.TEMPLATE > \
    /usr/share/obs/overview/index.html

if [ ! -z "$OBS_BACKEND_HOST" ]; then
    sed -i s/"source_host: localhost"/"source_host: ${OBS_BACKEND_HOST}"/g /etc/obs/api/config/options.yml
fi

# Set up msmtp if a configuration is supplied
if [ -f /run/secrets/msmtprc ]
then
    ln -sf /run/secrets/msmtprc /etc/msmtprc
fi

# Set up SSO auth if a configuration is supplied
if [ -f /run/secrets/ssoauth ]
then
    ln -sf /run/secrets/ssoauth /usr/share/obs/api/config/auth.yml
fi

if [ -n "$OBS_SESSION_LIFETIME_DAYS" ]
then
    sed -i 's/:expire_after => .*$/:expire_after => '"$OBS_SESSION_LIFETIME_DAYS"'.day/' \
        /usr/share/obs/api/config/initializers/session_store.rb
fi

echo "Setup rails app. Skip duplicate database error if database already exist."
/usr/share/obs/api/script/rake-tasks.sh setup
service obsapidelayed start
