#!/bin/sh

# Make sure there are no stale files from previous runs
rm -rfv /run/* /var/cache/obs/tmp/pids/*
mkdir -m a=rwxt /run/lock

/opt/configure-db.sh
/opt/configure-sso.py
/opt/configure-app.sh

/usr/share/obs/api/script/rake-tasks.sh serve -p 3000 -w ${OBS_FRONTEND_WORKERS:-4}
