#!/bin/sh -e

API_ROOT=/usr/share/obs/api

run_in_api () {

	export RAILS_ENV="production"
	cd $API_ROOT
	chroot --userspec=www-data:www-data --skip-chdir / bundle exec "$@"
}

case "$1" in
	setup)

	# Refine permissions for rails app.
	chown www-data:root /usr/share/obs/api/config/environment.rb
	chown -R www-data:www-data /var/log/obs/
	chown -R www-data:www-data /var/cache/obs/tmp/
	chown -R www-data:www-data /usr/share/obs/api/db
	chown -R www-data:www-data /usr/share/obs/api/public
	chown www-data:www-data /etc/obs/api/config/production.sphinx.conf
	chown obsapi:www-data /etc/obs/api/config/database.yml
	chmod 440 /etc/obs/api/config/database.yml

	touch /var/log/obs/backend_access.log /var/log/obs/production.log
	chmod 664 /var/log/obs/*.log
	chown obsapi:www-data /var/log/obs/backend_access.log
	chown obsapi:www-data /var/log/obs/production.log

	# Generate Gemfile.lock file.
	cd /usr/share/obs/api
	rm -f Gemfile.lock
	rm -f .bundle/config
	bundle --local

	# Setup database
	RAILS_ENV=production bundle exec rake db:create >> log/db_setup.log
	RAILS_ENV=production bundle exec rake db:setup >> log/db_setup.log

	export BUNDLE_WITHOUT=test:assets:development
	export BUNDLE_FROZEN=1
	bundle config --local frozen 1
	bundle config --local without test:assets:development

	run_in_api rake assets:precompile RAILS_ENV=production RAILS_GROUPS=assets
	run_in_api rake ts:index

	# Start up obsapidelayed
	if [ -x /usr/sbin/invoke-rc.d ]; then
            invoke-rc.d obsapidelayed restart 3>/dev/null || true && \
	    echo "obsapidelayed restarted."
        else
            /etc/init.d/obsapidelayed restart 3>/dev/null || true && \
	    echo "obsapidelayed restarted."
        fi
	;;


	serve)
	shift
	run_in_api puma "$@"
	;;

    migrate)
	# Migrade the database
	cd $API_ROOT
	RAILS_ENV=production bundle exec rake db:migrate >> log/db_migrate.log
	;;
    *)
	echo "Usage: $0 {setup|migrate}"
	exit 1
    ;;
esac
