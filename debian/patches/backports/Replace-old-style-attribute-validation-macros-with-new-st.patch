From: David Kang <gerheman@gmail.com>
Date: Tue, 17 Jan 2017 18:57:43 +0100
Subject: Replace old-style attribute validation macros with new-style ones

Was: [ci][api] Enable Rails/Validation rubocop cop

Checks for the use of old-style attribute validation macros.

Cherry-picked from 5524ffccb72a5f3c5ea3e255a0b76fe8b0225904
---
 src/api/app/models/architecture.rb |  4 +--
 src/api/app/models/user.rb         | 50 +++++++++++++++++++-------------------
 2 files changed, 27 insertions(+), 27 deletions(-)

diff --git a/src/api/app/models/architecture.rb b/src/api/app/models/architecture.rb
index ec81c56..e6c39da 100644
--- a/src/api/app/models/architecture.rb
+++ b/src/api/app/models/architecture.rb
@@ -18,8 +18,8 @@ class Architecture < ActiveRecord::Base
   scope :available, -> { where(available: 1) }
 
   #### Validations macros
-  validates_uniqueness_of :name
-  validates_presence_of :name
+  validates :name, uniqueness: true
+  validates :name, presence: true
 
   #### Class methods using self. (public and then private)
 
diff --git a/src/api/app/models/user.rb b/src/api/app/models/user.rb
index 7e45bba..6ac6eba 100644
--- a/src/api/app/models/user.rb
+++ b/src/api/app/models/user.rb
@@ -346,12 +346,6 @@ class User < ActiveRecord::Base
 
   # Model Validation
 
-  validates_presence_of :login, :email, :password, :password_hash_type, :state,
-                        :message => 'must be given'
-
-  validates_uniqueness_of :login,
-                          :message => 'is the name of an already existing user.'
-
   # Overriding this method to do some more validation: Password equals
   # password_confirmation, state an password hash type being in the range
   # of allowed values.
@@ -373,22 +367,28 @@ class User < ActiveRecord::Base
     end
   end
 
-  validates_format_of :login,
-                      :with => %r{\A[\w \$\^\-\.#\*\+&'"]*\z},
-                      :message => 'must not contain invalid characters.'
-  validates_length_of :login,
-                      :in => 2..100, :allow_nil => true,
-                      :too_long => 'must have less than 100 characters.',
-                      :too_short => 'must have more than two characters.'
+  validates :login, :email, :password, :password_hash_type, :state,
+            presence: { message: 'must be given' }
+
+  validates :login,
+            uniqueness: { message: 'is the name of an already existing user.' }
+
+  validates :login,
+            format: { with:    %r{\A[\w \$\^\-\.#\*\+&'"]*\z},
+                      message: 'must not contain invalid characters.' }
+  validates :login,
+            length: { in: 2..100, allow_nil: true,
+            too_long: 'must have less than 100 characters.',
+            too_short: 'must have more than two characters.' }
 
   # We want a valid email address. Note that the checking done here is very
   # rough. Email adresses are hard to validate now domain names may include
   # language specific characters and user names can be about anything anyway.
   # However, this is not *so* bad since users have to answer on their email
   # to confirm their registration.
-  validates_format_of :email,
-                      :with => %r{\A([\w\-\.\#\$%&!?*\'\+=(){}|~]+)@([0-9a-zA-Z\-\.\#\$%&!?*\'=(){}|~]+)+\z},
-                      :message => 'must be a valid email address.'
+  validates :email,
+            format: { with:    %r{\A([\w\-\.\#\$%&!?*\'\+=(){}|~]+)@([0-9a-zA-Z\-\.\#\$%&!?*\'=(){}|~]+)+\z},
+                      message: 'must be a valid email address.' }
 
   # We want to validate the format of the password and only allow alphanumeric
   # and some punctiation characters.
@@ -396,21 +396,21 @@ class User < ActiveRecord::Base
   # has not been stored yet and it has actually been set at all. Make sure you
   # include this condition in your :if parameter to validates_format_of when
   # overriding the password format validation.
-  validates_format_of :password,
-                      :with => %r{\A[\w\.\- !?(){}|~*]+\z},
-                      :message => 'must not contain invalid characters.',
-                      :if => Proc.new { |user| user.new_password? and not user.password.nil? }
+  validates :password,
+            format: { with:    %r{\A[\w\.\- /+=!?(){}|~*]+\z},
+                      message: 'must not contain invalid characters.',
+                      if:      Proc.new { |user| user.new_password? && !user.password.nil? } }
 
   # We want the password to have between 6 and 64 characters.
   # The length must only be checked if the password has been set and the record
   # has not been stored yet and it has actually been set at all. Make sure you
   # include this condition in your :if parameter to validates_length_of when
   # overriding the length format validation.
-  validates_length_of :password,
-                      :within => 6..64,
-                      :too_long => 'must have between 6 and 64 characters.',
-                      :too_short => 'must have between 6 and 64 characters.',
-                     :if => Proc.new { |user| user.new_password? and not user.password.nil? }
+  validates :password,
+            length: { within:    6..64,
+                      too_long:  'must have between 6 and 64 characters.',
+                      too_short: 'must have between 6 and 64 characters.',
+                      if:        Proc.new { |user| user.new_password? && !user.password.nil? } }
 
   class << self
     def current
